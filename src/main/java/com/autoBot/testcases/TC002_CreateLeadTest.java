package com.autoBot.testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create a Lead in leaftaps";
		author = "Ahalya";
		category = "smoke";
		excelFileName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String uName, String pwd, String cName, String fName, String lName) throws InterruptedException
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCrmSfaLink()
		.clickLeadsTab()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.verifyLeadFirstName(fName);
	}
	
}
