package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;



public class MyLeadsPage extends Annotations {
	
	@When("Click on the Create Lead")
	public CreateLeadPage clickCreateLead() {
		//driver.findElementByLinkText("Create Lead").click();
		WebElement eleCreateLead=locateElement("link","Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();	
		}
		
}
