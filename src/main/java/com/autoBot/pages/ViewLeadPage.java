package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;


 //@Then("Verify the Lead Created is with firstName (.*)")

public class ViewLeadPage extends Annotations {
	
	    public ViewLeadPage verifyLeadFirstName(String data) throws InterruptedException {
		
		Thread.sleep(10);
		
		WebElement eleVerifyfName=locateElement("xpath","//span[@id='viewLead_firstName_sp']");
		String text= getElementText(eleVerifyfName);
		
		if (text.contains(data)) {
			System.out.println("First Name matches" + "\t" + eleVerifyfName);
		}

		else

			System.out.println("Name doesn't match" + "\t" + eleVerifyfName);
		return this;
	}
	
}
