package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.When;



public class MyHomePage extends Annotations {
	
	@When("Click on the Leads Tab")
	public MyLeadsPage clickLeadsTab() 
	{
		WebElement eleLead=locateElement("link","Leads");
		click(eleLead);
		return new MyLeadsPage();
	}
}
