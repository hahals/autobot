package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
		PageFactory.initElements(driver, this); 
	}


	@And("Enter the password as (.*)")
	public CreateLeadPage enterPassWord(String data) {
		WebElement elePassWord = locateElement("id", "password");
		clearAndType(elePassWord, data); 
		return this; 
	}

	@When("Click on the login button")
	public HomePage clickLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);  
		return new HomePage();
	}
	
	@And("Enter the Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String data)
	{
		WebElement eleCompanyName=locateElement("xpath","(//input[@name='companyName'])[2]");
		clearAndType(eleCompanyName, data);
		return this;
	}
	
	@And("Enter the First Name as (.*)")
	public CreateLeadPage enterFirstName(String data)
	{
		WebElement eleFirstName=locateElement("xpath","//input[@id='createLeadForm_firstName']");
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@And("Enter the Last Name as (.*)")
	public CreateLeadPage enterLastName(String data)
	{
		WebElement eleLastName=locateElement("xpath","//input[@id='createLeadForm_lastName']");
		clearAndType(eleLastName, data);
		return this;
	}
	
	@And("Click the Create Lead Button")
	public ViewLeadPage clickCreateLeadButton()
	{
		WebElement eleCreateLeadButton=locateElement("xpath","//input[@name='submitButton']");
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}

}







